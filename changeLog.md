Ce document sera maintenu dans la mesure du possible [en suivant ces recommandations](https://keepachangelog.com/fr/1.0.0/)

# 201810a_planStockage
+ **Added** :
    + fichiers de configuration des dégradés dans CloudCompare (dossier `//id.../0_assets`)
    + fichiers de configuration des éclairages virtuels dans Krita (dossier `//id.../0_assets`)
    + document-type pour la description géométrale d'un bloc de pierre orné (dossier `//id.../5_descriptionGeometrale/6_blocPierre`)
    + fichier pour créer facilement les graticules lors de la description géométrale (dossier `///_descriptionGeometrale/definitionGraticules.ods`)
    + fichier pour déterminer facilement tous les systèmes de coordonnées locaux, à toutes les échelles de représentation (dossier `//0_assets/defifintionSCL.ods`)

+ **Changed** :
    + déplacement du fichier `CalculDimImage.ods` du répertoire `//id.../0-assets` vers `//id.../5_descriptionGeometrale`
    + déplacement du fichier `padonnees_201808a.yml` du répertoire `//id.../0-assets` vers `//id...`
    + réorganisation du répertoire `0_assets`
    + réorganisation du plan de nommage (fichiers présents dans `0_assets_planNommage`) pour la partie "socle commun de représentation"

+ **Deprecated**
+ **Removed**
+ **Fixed**
+ **Security**