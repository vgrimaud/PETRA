PETRA
===

PETRA -- Protocole d'Exploitation des représentations TRidimensionnelles en Archéologie -- est **une méthode pour étudier les vestiges archéologiques numérisés en 3D**, et plus spécifiquement l'art pariétal. Cet espace gitlab permet de diffuser les aspects du plan de stockage et de nommage des fichiers produits au cours d'une étude, étant eux-mêmes compris dans ce qu'on appelle un **plan de gestion de données** (data managment plan). 

S'il est adapté à l'étude des oeuvres pariétales, il devrait aussi convenir à l'ensemble des études 3D en archéologie moyennant quelques adaptations.

## Premiers pas

Pour une approche approfondie de ce qu'est un plan de gestion de données et découvrir tous ses aspects, se référer au document [*Réaliser un plan de gestion de données*](http://cache.media.education.gouv.fr/file/Comment_Participer/36/0/realiser_un_dmp_406360.pdf).

Ce document vise à fournir une arborescence de dossier compatible avec les objectifs d'une étude de l'art pariétal avec les outils de représentation 3D.

Pour utiliser cette méthode de travail, téléchargez la dernière version de l'arborescence de dossier, et décompressez le contenu dans votre ordinateur, à l'endroit adéquat.


## Documentation

Pour accéder à la description détaillée de la méthode de travail PETRA, suivre [ce lien (en construction)]().


## Contribuer

Pour suggérer une amélioration ou remonter une incohérence, veuillez [suivre la procédure indiquée dans ce document](https://github.com/necolas/issue-guidelines/blob/master/CONTRIBUTING.md) (en anglais).


## Versions

Le nommage des versions suit le schéma : `AnnéeMois_planStockage.zip`

+ [201810a_planStockage](201808a_planStockage.zip) : dernière version
+ [201808a_planStockage](201808a_planStockage.zip)
+ [201808_planStockage](201808_planStockage.zip)
+ [201802_planStockage](201802_planStockage.zip)

La liste des modifications apportées est consultable sur ce [ChangeLog](changeLog.md)


## Mainteneur

+ [Valentin Grimaud](http://www.univ-nantes.fr/site-de-l-universite-de-nantes/valentin-grimaud--1440141.kjsp)


## Licence

Le contenu de ce répertoire est diffusé sous [licence CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr).

## Crédits

La réflexion est conduite dans le cadre du programme de recherche "Corpus des signes gravés" dirigé par S. Cassen.
